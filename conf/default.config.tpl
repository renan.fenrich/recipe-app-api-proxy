server {
  listen ${LISTEN_PORT};

  location /static {
    alias /vol/static;
  }

  location / {
    uwsgi_pass ${APP_HOST}:${APP_PORT}

    client_max_body_size 10M;

    include /etc/nginx/uwsgi_params.conf
  }
}
