FROM nginxinc/nginx-unprivileged:1-alpine

COPY ./conf/default.config.tpl /etc/nginx/conf.d/default.conf
COPY ./conf/uwsgi_params.conf /etc/nginx//uwsgi_params.conf

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.tlp
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./conf/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint,sh"]
